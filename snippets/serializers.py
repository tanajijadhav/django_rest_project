from rest_framework import serializers
from snippets.models import Snippet, LANGUAGE_CHOICES, STYLE_CHOICES


class SnippetSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="snippets:snippet_detail")
    class Meta:
        model = Snippet
        fields = ('__all__')
        #fields = ('id', 'url','title', 'code', 'linenos', 'language', 'style')